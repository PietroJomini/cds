#ifndef __LL__
#define __LL__

#include <stdlib.h>
#include <stdbool.h>

struct _node {

    struct _node *prev;
    struct _node *next;

    void *data;

    bool _toFree;

};

typedef struct _node node;

node LL_unlinkedNode(void* data);
node LL_nth(node *head, unsigned int index, unsigned int *resStatus);
node LL_pop(node *head);
node *LL_first(node *nd);
node *LL_last(node *nd);

void LL_setData(node *nd, void *data);
void LL_push(node *head, void *data);
void LL_forEach(node *head, void (*f)(node *));
void LL_insertAfter(node *stp, node *newNode);
void LL_insertBefore(node *stp, node *newNode);

void LL_freeNode(node *nd);
void LL_free(node *head);

#include "LL.c"

#endif // !__LL__