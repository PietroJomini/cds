
void LL_setData(node *nd, void *data) {

    if (data == NULL) nd->data = NULL;
    else {
        nd->data = malloc(sizeof(*data));
        memcpy(nd->data, &(*data), sizeof(&(*data)));
    }

    return;
}

node LL_unlinkedNode(void* data) {

    node nd;

    nd.prev = NULL;
    nd.next = NULL;

    nd._toFree = false;

    LL_setData(&nd, data);

    return nd;
}

void LL_push(node *head, void *data) {

    node *lst = LL_last(head);

    node *newNode = malloc(sizeof(node));
    newNode->next = NULL;
    newNode->prev = lst;
    newNode->_toFree = true;

    LL_setData(newNode, data);

    lst->next = newNode;

    return;
}

void LL_forEach(node *head, void (*f)(node *)) {

    node *curr = LL_first(head);
    while (curr != NULL) {
        (*f)(curr);    
        curr = curr->next;
    }

    return;
}

void LL_insertAfter(node *stp, node *newNode) {

    newNode->next = stp->next;
    if (stp->next != NULL) stp->next->prev = newNode;
    stp->next = newNode;
    newNode->prev = stp;

    return;
}

void LL_insertBefore(node *stp, node *newNode) {

    if (stp->prev != NULL) stp->prev->next = newNode;
    newNode->prev = stp->prev;
    newNode->next = stp;
    stp->prev = newNode;

    return;
}

node LL_pop(node *head) {

    node poppedNode;

    node *lst = LL_last(head);

    lst->prev->next = NULL;
    memcpy(&poppedNode, lst, sizeof(node));

    LL_freeNode(lst);

    return poppedNode;
}

node *LL_first(node *nd) {
    while (nd->prev != NULL) nd = nd->prev;
    return nd;
}

node *LL_last(node *nd) {
    while (nd->next != NULL) nd = nd->next;
    return nd;
}

node LL_nth(node *head, unsigned int index, unsigned int *resStatus) {

    unsigned int _index = 0;
    node *fst = LL_first(head);
    node res;

    while (fst != NULL && _index++ < index) fst = fst->next;

    if (fst == NULL) { if (resStatus != NULL) *resStatus = -1; }
    else {
        if (resStatus != NULL) *resStatus = 0;
        memcpy(&res, fst, sizeof(node));
    }

    return res;
}

void LL_freeNode(node *nd) {

    free(nd->data);
    if (nd->_toFree) free(nd);

    return;
}

void LL_free(node *head) {

    node *fst = LL_first(head);
    LL_forEach(fst, LL_freeNode);

    return;
}