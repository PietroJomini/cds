#ifndef __GRAPH__
#define __GRAPH__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define ALLOC_N_ITEM 10

typedef struct _node {
    void *data;
    uint64_t key;
} node;

typedef struct _edge {
    node *a, *b;
    void *data;
} edge;

typedef struct _graph {
    node *nodes;
    edge *edges;
    size_t nNodes, nEdges;
    size_t aNodes, aEdges;
} graph;

graph GRP_newGraph(size_t nNodes, size_t nEdges);
graph *GRP_node(graph *gpr, uint64_t key);

int GRP_addNode(graph *grp, uint64_t key, void *data);
int GRP_addEdge(graph *grp, uint64_t aKey, uint64_t bKey);

bool GRP_isNodeExistent(graph *grp, uint64_t key);
bool GRP_adjacent(graph *grp, uint64_t aKey, uint64_t bKey);

void GRP_removeEdge(graph *grp, uint64_t aKey, uint64_t bKey);
void GRP_removeNode(graph *grp, uint64_t key);

uint64_t *neighbours(graph *grp, uint64_t key, int *size);

#include "./Graph.c"

#endif // !__GRAPH__