#include "./Graph.h"

graph GRP_newGraph(size_t nNodes, size_t nEdges) {

    graph grp;

    grp.nNodes = 0;
    grp.nEdges = 0;

    grp.nodes = malloc(sizeof(node) * nNodes);
    grp.edges = malloc(sizeof(edge) * nEdges);

    grp.aNodes = nNodes;
    grp.aEdges = nEdges;

    return grp;
}

int GRP_addNode(graph *grp, uint64_t key, void *data) {

    if (GRP_isNodeExistent(grp, key)) return -1;

    grp->nNodes += 1;

    if (grp->nNodes >= grp->aNodes) {
        grp->aNodes += ALLOC_N_ITEM;
        grp->nodes = realloc(grp->nodes, sizeof(node) * grp->aNodes);
    }

    grp->nodes[grp->nNodes - 1].key = key;
    grp->nodes[grp->nNodes - 1].data = malloc(sizeof(*(&data)));
    memcpy(&(grp->nodes[grp->nNodes - 1].data), *(&data), sizeof(*(&data)));

    return 0;
}

graph *GRP_node(graph *grp, uint64_t key) {

    node *nd = NULL;

    uint8_t i = 0;
    while (i < grp->nNodes && nd == NULL) {
        if (grp->nodes[i].key == key) nd = &(grp->nodes[i]);
        i += 1;
    }

    return nd;
}

bool GRP_isNodeExistent(graph *grp, uint64_t key) {

    uint8_t i = 0;
    bool existent = false;
    while (i < grp->nNodes && !existent) {
        if (grp->nodes[i].key == key) existent = true;
        i += 1;
    }

    return existent;
}

int GRP_addEdge(graph *grp, uint64_t aKey, uint64_t bKey) {

    if (GRP_adjacent(grp, aKey, bKey)) return -1;

    grp->nEdges += 1;

    if (grp->nEdges >= grp->aEdges) {
        grp->aEdges += ALLOC_N_ITEM;
        grp->edges = realloc(grp->edges, sizeof(node) * grp->aEdges);
    }

    grp->edges[grp->nEdges - 1].a = GRP_node(grp, aKey);
    grp->edges[grp->nEdges - 1].b = GRP_node(grp, bKey);

    return 0;
}

bool GRP_adjacent(graph *grp, uint64_t aKey, uint64_t bKey) {

    uint8_t i = 0;
    bool existent = false;
    while (i < grp->nEdges && !existent) {
        if ((grp->edges[i].a->key == aKey && grp->edges[i].b->key == bKey) ||
            (grp->edges[i].a->key == bKey && grp->edges[i].b->key == aKey)) 
            existent = true;
        i += 1;
    }

    return existent;
}

void GRP_removeEdge(graph *grp, uint64_t aKey, uint64_t bKey) {

    uint8_t i = 0;
    while (i < grp->nEdges) {
        if ((grp->edges[i].a->key == aKey && grp->edges[i].b->key == bKey) ||
            (grp->edges[i].a->key == bKey && grp->edges[i].b->key == aKey)) {
                grp->nEdges -= 1;
                uint8_t ii = i;
                while (ii < grp->nEdges) {
                    grp->edges[ii] = grp->edges[ii + 1];
                    ii += 1;
                }
            }
            
        i += 1;
    }

    return;
}

void GRP_removeNode(graph *grp, uint64_t key) {

    uint8_t i = 0;
    while (i < grp->nNodes) {
        if (grp->nodes[i].key == key) {
            uint8_t ii = 0;
            while (ii < grp->nEdges) {
                uint8_t iii = 0;
                while (iii < grp->nNodes) {
                    if (GRP_adjacent(grp, key, grp->nodes[iii].key))
                        GRP_removeEdge(grp, key, grp->nodes[iii].key);
                    iii += 1;
                }
                ii += 1;
            }
            grp->nNodes -= 1;
            ii = i;
            while (ii < grp->nNodes) {
                grp->nodes[ii] = grp->nodes[ii + 1];
                ii += 1;
            }
        }
        i += 1;
    }

    return;
}

uint64_t *neighbours(graph *grp, uint64_t key, int *size) {

    uint64_t *ngh = NULL;
    *size = 0;

    uint8_t i = 0;
    while (i < grp->nEdges) {
        if (grp->edges[i].a->key == key) {
            *size += 1;
            ngh = realloc(ngh, *size * sizeof(uint64_t));
            ngh[*size - 1] = grp->edges[i].b->key;
        } else if (grp->edges[i].b->key == key) {
            *size += 1;
            ngh = realloc(ngh, *size * sizeof(uint64_t));
            ngh[*size - 1] = grp->edges[i].a->key;
        }
        i += 1;
    }

    return ngh;
}