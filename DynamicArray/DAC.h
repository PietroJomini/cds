#ifndef __DAC__
#define __DAC__

#include <stdlib.h>

#define DAC(type)               \
    struct {                    \
        type *_;                \
        unsigned int length;    \
    }

#define DAC_init()      \
    {                   \
        ._ = NULL,      \
        .length = 0     \
    }

#ifdef __GNUC__
#define DAC_push(array, element)                                        \
    ({                                                                  \
        array.length += 1;                                              \
        array._ = realloc(array._, sizeof(*(array._)) * array.length);  \
        array._[array.length - 1] = element;                            \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_pop(array)                                                  \
    ({                                                                  \
        array.length -= 1;                                              \
        array._[array.length];                                          \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_free(array)     \
    ({                      \
        free(array._);      \
        array._ = NULL;     \
        array.length = 0;   \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_forEach(array, cBack)                                           \
    ({                                                                      \
        for (unsigned int _index = 0; _index < array.length; _index += 1)   \
            (*cBack)(array._[_index], _index, array._);                     \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_indexOf(array, key, cmp)                                \
    ({                                                              \
        unsigned int i = 0;                                         \
        while (i < array.length && cmp(array._[i], key)) i += 1;    \
        if (i == array.length) i = -1;                              \
        i;                                                          \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_reverse(array)                                                          \
    ({                                                                              \
        void *_tmp = malloc(sizeof(*(array._)));                                    \
        for (unsigned int _index = 0; _index < array.length / 2; _index += 1) {     \
            _tmp = array._[_index];                                                 \
            array._[_index] = array._[array.length - _index - 1];                   \
            array._[array.length - _index - 1] = _tmp;                              \
        }                                                                           \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_clone(arrayTo, arrayFrom)                                                   \
    ({                                                                                  \
        arrayTo._ = NULL;                                                               \
        arrayTo._ = realloc(arrayTo._, sizeof(*(arrayFrom._)) * arrayFrom.length);      \
        memcpy(arrayTo._, arrayFrom._, sizeof(*(arrayFrom._)) * arrayFrom.length);      \
        arrayTo.length = arrayFrom.length;                                              \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_slice(array, begin, end)                                                                    \
    ({                                                                                                  \
        unsigned int _rval = -1;                                                                        \
        if (begin >= 0 && begin <= end && begin < array.length) {                                       \
            for (unsigned int _index = 0; _index < end && _index + begin < array.length; _index += 1)   \
                array._[_index] = array._[_index + begin];                                              \
            array.length = (end <= array.length) ? end - begin : array.length - begin;                  \
            _rval = 1;                                                                                  \
        } ;                                                                                             \
        _rval;                                                                                          \
    })
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_shift(array)                                                        \
    ({                                                                          \
        array.length -= 1;                                                      \
        for (unsigned int _index = 0; _index < array.length - 1; _index += 1)   \
            array._[_index] = array._[_index + 1];                              \
    })  
#endif // __GNUC__

#ifdef __GNUC__
#define DAC_unshift(array, val)                                                 \
    ({                                                                          \
        array.length += 1;                                                      \
        array._ = realloc(array._, sizeof(*(array._)) * array.length);          \
        for (unsigned int _index = array.length - 1; _index > 0; _index -= 1)   \
            array._[_index] = array._[_index - 1];                              \
        array._[0] = val;                                                       \
    })  
#endif // __GNUC__

#endif // !__DAC__
